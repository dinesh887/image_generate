import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonReader;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;


/**
 * Created by dineshpriyashantha on 11/19/15.
 */
public class GenerateImage
{
    public static void main(String[] args) {
        Gson gson = new Gson();
        String result;
        try {
            Map<String, Object> map = new HashMap<String, Object>();
            map = gson.fromJson(args[0], map.getClass());
            BufferedImage ire;
            BufferedImage ire1;
            ire = WebImage.create(map.get("url").toString(), 740, 600);
            ire1 = WebImage.createThumb(map.get("url").toString(), 330, 300);
            ImageIO.write(ire, "gif", new File(map.get("fileName").toString()));
            ImageIO.write(ire1, "gif", new File(map.get("thumbFileName").toString()));
            result = "1";
        } catch (Exception e) {
            e.printStackTrace();
            result = "error "+e.getMessage();
        }
        System.out.print(result);

    }
}
