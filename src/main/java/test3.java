import gui.ava.html.image.util.FormatNameUtil;
import gui.ava.html.image.util.SynchronousHTMLEditorKit;
import gui.ava.html.link.LinkHarvester;
import gui.ava.html.link.LinkInfo;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.List;
import java.awt.image.BufferedImage;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.util.*;

/**
 * Created by dineshpriyashantha on 11/20/15.
 */
public class test3 {

    private JEditorPane editorPane;
    static final Dimension DEFAULT_SIZE = new Dimension(800, 800);
    private String fileName;
    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }


    public test3() {
        editorPane = createJEditorPane();
    }

    public ComponentOrientation getOrientation() {
        return editorPane.getComponentOrientation();
    }

    public void setOrientation(ComponentOrientation orientation) {
        editorPane.setComponentOrientation(orientation);
    }

    public Dimension getSize() {
        return editorPane.getSize();
    }

    public void setSize(Dimension dimension) {
        editorPane.setSize(dimension);
    }

    public void loadUrl(URL url) {
        try {
            editorPane.setPage(url);
        } catch (IOException e) {
            throw new RuntimeException(String.format("Exception while loading %s", url), e);
        }
    }

    public void loadUrl(String url) {
        try {
            editorPane = new JEditorPane(url);
        } catch (IOException e) {
            throw new RuntimeException(String.format("Exception while loading %s", url), e);
        }
    }

    public void loadHtml(String html) {
        editorPane.setText(html);
        onDocumentLoad();
    }

    public String getLinksMapMarkup(String mapName) {
        final StringBuilder markup = new StringBuilder();
        markup.append("<map name=\"").append(mapName).append("\">\n");
        for (LinkInfo link : getLinks()) {
            final java.util.List<Rectangle> bounds = link.getBounds();
            for (Rectangle bound : bounds) {
                final int x1 = (int) bound.getX();
                final int y1 = (int) bound.getY();
                final int x2 = (int) (x1 + bound.getWidth());
                final int y2 = (int) (y1 + bound.getHeight());
                markup.append(String.format("<area coords=\"%s,%s,%s,%s\" shape=\"rect\"", x1, y1, x2, y2));
                for (Map.Entry<String, String> entry : link.getAttributes().entrySet()) {
                    String attName = entry.getKey();
                    String value = entry.getValue();
                    markup.append(" ").append(attName).append("=\"").append(value.replace("\"", "&quot;")).append("\"");
                }
                markup.append(">\n");
            }
        }
        markup.append("</map>\n");
        return markup.toString();
    }

    public java.util.List<LinkInfo> getLinks() {
        final LinkHarvester harvester = new LinkHarvester(editorPane);
        return harvester.getLinks();
    }

    public void saveAsHtmlWithMap(String file, String imageUrl) {
        saveAsHtmlWithMap(new File(file), imageUrl);
    }

    public void saveAsHtmlWithMap(File file, String imageUrl) {
        FileWriter writer = null;
        try {
            writer = new FileWriter(file);
            writer.append("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">\n");
            writer.append("<html>\n<head></head>\n");
            writer.append("<body style=\"margin: 0; padding: 0; text-align: center;\">\n");
            final String htmlMap = getLinksMapMarkup("map");
            writer.write(htmlMap);
            writer.append("<img border=\"0\" usemap=\"#map\" src=\"");
            writer.append(imageUrl);
            writer.append("\"/>\n");
            writer.append("</body>\n</html>");
        } catch (IOException e) {
            throw new RuntimeException(String.format("Exception while saving '%s' html file", file), e);
        } finally {
            if (writer != null) {
                try {
                    writer.close();
                } catch (IOException ignore) {
                }
            }
        }

    }

    public void saveAsImage() {
        saveAsImage(new File(this.getFileName()));
    }

    public void saveAsImage(File file) {

        BufferedImage img = getBufferedImage();

        try {
            final String formatName = FormatNameUtil.formatForFilename(file.getName());
//            ImageIO.write(img, formatName, file);
            //BufferedImage img = new BufferedImage(1, 1, BufferedImage.TYPE_INT_ARGB);

            ImageIO.write(img, formatName, file);
                //ImageIO.write(img, "png", new File("Text.png"));

        } catch (IOException e) {
            throw new RuntimeException(String.format("Exception while saving '%s' image", file), e);
        }
    }

    protected void onDocumentLoad() {
    }

    public Dimension getDefaultSize() {
        return DEFAULT_SIZE;
    }

    public BufferedImage getBufferedImage() {
        Dimension prefSize = editorPane.getPreferredSize();
        BufferedImage img = new BufferedImage(1024, editorPane.getPreferredSize().height, BufferedImage.TYPE_INT_ARGB);
//        Graphics graphics = img.getGraphics();
        Graphics2D g2d = img.createGraphics();
        Font font = new Font("Arial", Font.PLAIN, 48);
        g2d.setFont(font);
        FontMetrics fm = g2d.getFontMetrics();
//           int width = fm.stringWidth(file);
//        int height = fm.getHeight();
//        g2d.dispose();

//        img = new BufferedImage(800, 800, BufferedImage.TYPE_INT_ARGB);
//        g2d = img.createGraphics();
//        g2d.setRenderingHint(RenderingHints.KEY_ALPHA_INTERPOLATION, RenderingHints.VALUE_ALPHA_INTERPOLATION_QUALITY);
//        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
//        g2d.setRenderingHint(RenderingHints.KEY_COLOR_RENDERING, RenderingHints.VALUE_COLOR_RENDER_QUALITY);
//        g2d.setRenderingHint(RenderingHints.KEY_DITHERING, RenderingHints.VALUE_DITHER_ENABLE);
//        g2d.setRenderingHint(RenderingHints.KEY_FRACTIONALMETRICS, RenderingHints.VALUE_FRACTIONALMETRICS_ON);
//        g2d.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
//        g2d.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
//        g2d.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_PURE);
//        g2d.setFont(font);
//        fm = g2d.getFontMetrics();
        g2d.setColor(Color.RED);
//        //g2d.drawString(text, 0, fm.getAscent());
//        g2d.dispose();
        editorPane.setSize(prefSize);
        editorPane.paint(g2d);
        return img;
    }

    protected JEditorPane createJEditorPane() {
        final JEditorPane editorPane = new JEditorPane();
        editorPane.setSize(getDefaultSize());
        editorPane.setEditable(false);
        final SynchronousHTMLEditorKit kit = new SynchronousHTMLEditorKit();
        editorPane.setEditorKitForContentType("text/html; charset=UTF-8", kit);
        editorPane.setContentType("text/html; charset=UTF-8");
        editorPane.addPropertyChangeListener(new PropertyChangeListener() {
            public void propertyChange(PropertyChangeEvent evt) {
                if (evt.getPropertyName().equals("page")) {
                    onDocumentLoad();
                }
            }
        });
        return editorPane;
    }
}
